#! /usr/bin/env python3.8


import subprocess
import json
from pathlib import Path


class Location():
    """File locations paths."""

    __base_path = '~/.config/updates_notifier/'
    __updateslist = 'updateslist'
    __extension = '.txt'
    __db = 'repos.json'

    def __make_path(self, path_str):
        """Return string as a path.

        Input: String
        Output: Path
        """

        return Path.resolve(Path.expanduser(Path(path_str)))

    def get_base(self):
        """Return base path.

        Output: Path
        """

        path = self.__make_path(self.__base_path)
        return path

    def get_extension(self, repo_name):
        """Return updates file path.

        Input: String
        Output: Path
        """

        base = self.get_base()
        path = base.joinpath(
            Path(self.__updateslist), Path(repo_name + self.__extension))
        return path

    def get_db(self):
        """Return database file path.

        Output: Path
        """

        base = self.get_base()
        path = base.joinpath(Path(self.__db))
        return path


class UpdateList():
    """Save given repository updates to file."""

    def __init__(self, package):
        """Initialize UpdateList.

        Has association with Location.
        Input: Package
        """

        self.__location = Location()
        self.__base_path = self.__location.get_base()
        self.__package = package
        self.__path = self.__check_path()

    def __check_path(self):
        """Make sure folders exist and return updates file path.

        Output: Path
        """

        repo_name = self.__package.get_name().lower()

        base = self.__location.get_base()
        if not Path.exists(base):
            raise FileNotFoundError(
                f'Folder "{self.__base_path}" is not found')

        path = self.__location.get_extension(repo_name)
        return path

    def save_list(self):
        """Save repositorys updates into file."""

        updates_list = self.__package.get_updates()
        with open(self.__path, 'w') as updates_file:
            for line in updates_list:
                updates_file.write(line + '\n')


class PlainPackage():
    """Unvarying parts of packages."""

    def __init__(self):
        """Initialize PlainPackage.

        Has association with Location.
        """

        location = Location()
        db_path = location.get_db()
        self._repos = self.__read_db(db_path)

    def __read_db(self, db_path):
        """Reads database repository dictionary.

        Input: Path
        Output: dict
        """

        with open(db_path) as db:
            repos = json.load(db)
        return repos

    def _parse_list(self, updates):
        """Parse updates into a list.

        Input: String
        Output: list
        """

        update = updates.strip()
        return update.splitlines()

    def _process(self):
        """Run command that retrieves updates the repo.

        Output: String
        """
        updates = subprocess.run(self._repo_cmd, capture_output=True)
        return updates.stdout.decode('utf-8')

    def _pipe_process(self):
        """Run a piped command that retrieves updates the repo.

        Output: String
        """

        cmd = self._repo_cmd
        i_pipe = cmd.index('|')

        pre = cmd[:i_pipe]
        post = cmd[i_pipe+1:]

        pre = subprocess.Popen(pre, stdout=subprocess.PIPE)
        post = subprocess.check_output(
            post, stdin=pre.stdout)

        return post.stdout.decode('utf-8')

    def get_name(self):
        """Returns the repositorys name.

        Output: String
        """

        return self._repo_name

    def get_updates(self):
        """Use normal or piped processing based on command.

        Output: String
        """

        pipes = self._repo_cmd.count('|')
        if pipes == 0:
            updates = self._process()
        elif pipes == 1:
            updates = self._pipe_process()
        else:
            raise ValueError('Update command can have only one pipe')

        return self._parse_list(updates)


class Package(PlainPackage):
    """Varying parts of Package."""

    def __init__(self, repo_name):
        """Initialize Package."""

        super().__init__()

        if repo_name in self._repos:
            self._repo_cmd = self._repos[repo_name]
            self._repo_name = repo_name
        else:
            raise ValueError(f'repo "{repo_name}" is unknown')

        if repo_name == 'FLATPAK':
            self.__flatpak_override()

    def __flatpak_override(self):
        """Flatpak repository specific function decorators."""

        self._pipe_process = self.__override_process(
            self._pipe_process, (1, ))
        self._parse_list = self.__override_parse(
            self._parse_list, (slice(3, -2), ))

    def __override_parse(self, orig_func, del_lines):
        """Decorator for _parse_list().

        Allows slicing lines from updates_list.
        Input: (), tuple
        Output: ()
        """

        def wrapper(update):
            updates = orig_func(update)
            for del_line in del_lines:
                update = updates[del_line]
            return update

        return wrapper

    def __override_process(self, orig_func, err_codes):
        """Decorator for _process and _piped_process.

        Allows adding error handling.
        Input (), tuple
        Output: ()
        """

        def wrapper():
            try:
                updates = orig_func()
            except subprocess.CalledProcessError as e:
                if e.returncode in err_codes:
                    updates = e.stdout.decode('utf-8')
                else:
                    raise subprocess.CalledProcessError(f'{e}')

            return updates

        return wrapper


if __name__ == '__main__':
    arch = UpdateList(Package('ARCH'))
    arch.save_list()

    yay = UpdateList(Package('YAY'))
    yay.save_list()

    flatpak = UpdateList(Package('FLATPAK'))
    flatpak.save_list()
